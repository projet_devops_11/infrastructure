
## Provisionnement impossible avec ansible, VM UNREACHEABLE

- Générer une clef ssh
- Ajouter les VM dans le fichier etc/hosts
- Ne pas renseigner le mdp dans le ansible.cfg
- lancer la commande : ansible jenkins-server -i inventory.ini -m ping
- Une erreur "UNREACHEABLE" devrait apparaitre
- remettre le mot de passe de l'utilisateur vagrant dans le fichier ansible.cfg
- relancer la commande : ansible jenkins-server -i inventory.ini -m ping
- La commande devrait retourner un success.

https://www.youtube.com/watch?v=MQ1Y3Uz2RQ0

## Ajout des hosts

Ajouter dans le fichier etc/hosts les vm

## Impossible d'être root

- sudo passwd root
- choisir le mdp